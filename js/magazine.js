var url = 'http://ajinomoto.goship.co.id/';
// var url = 'http://localhost:8888/backend/public/';

$(document).ready(function () {
    getMagazine();
});

function getMagazine() {
    axios({
        method: 'get',
        url: url + 'api/majalah'
    }).then(res => {
        var wrapperList = document.getElementById('wrapperList');
        var data = res.data.data;
        for (i = 0; i < data.length; i++) {
            var childList = `<div class="col-md-4 col-lg-3 mt-1 mb-1"><div class="card card-borderless bg-transparent">
                <div class="card-body d-flex flex-column justify-content-start align-items-start">
                    <img onclick="openBook(${data[i].id}, '${url + data[i].cover}')" src="${url + data[i].cover}" alt="" class="shadow maga-cover mb-2">
                    <small>Title</small>
                    <h5 class="dotted">${data[i].judul}</h5>
                </div>
            </div></div>`

            wrapperList.innerHTML += childList;
        }
    }).catch(err => {
        console.log(err)
    })
}

function getListMagazine() {
    axios({
        method: 'get',
        url: url + 'api/majalah'
    }).then(res => {
        var magazineList = document.getElementById('magazineList');
        var data = res.data.data;
        for (i = 0; i < data.length; i++) {
            var list = `<div class="col-3">
                <div class="card-body pl-0 pr-0 d-flex flex-column justify-content-start align-items-start">
                    <img src="${url + data[i].cover}" class="maga-cover" alt="...">
                    <div class="d-flex flex-column mt-2">
                        <small>Title</small>
                        <h4 class="card-title">${data[i].judul}</h4>
                        <div class="btn-group mt-2">
                        <a href="#" class="btn btn-danger">Delete</a>
                        <a href="#" class="btn btn-secondary">Update</a>
                        </div>
                    </div>
                </div>
            </div>`

            magazineList.innerHTML += list;
        }
    }).catch(err => {
        console.log(err)
    })
}

function getMagazineById(id) {
    axios({
        method: 'get',
        url: url + `api/majalah/${id}`
    }).then(res => {
        var wrapperList = document.getElementById('wrapperList');
        var data = res.data.data;
        for (i = 0; i < data.length; i++) {
            var childList = `<div data-id="${data[i]}" class="card card-borderless bg-transparent">
                <div class="card-body d-flex flex-column justify-content-start align-items-start">
                    <img onclick="openBook(${data[i]})" src="${url + data[i].cover}" alt="" class="shadow maga-cover mb-2">
                    <h5 class="dotted">${data[i].judul}</h5>
                </div>
            </div>`

            wrapperList.innerHTML += childList;
        }
    }).catch(err => {
        console.log(err)
    })
}

function getHalamanMagazine(id) {
    axios({
        method: 'get',
        url: url + `api/majalah/${id}/majalah`
    }).then(res => {
        var wrapperList = document.getElementById('wrapperList');
        var data = res.data.data;
        for (i = 0; i < data.length; i++) {
            var childList = `<div data-id="${data[i]}" class="card card-borderless bg-transparent">
                <div class="card-body d-flex flex-column justify-content-start align-items-start">
                    <img onclick="openBook(this)" src="${url + data[i].cover}" alt="" class="shadow maga-cover mb-2">
                    <h5 class="dotted">${data[i].judul}</h5>
                </div>
            </div>`

            wrapperList.innerHTML += childList;
        }
    }).catch(err => {
        console.log(err)
    })
}

function openBook(id, cover) {
    axios({
        method: 'get',
        url: url + `api/majalah/${id}/halaman`
    }).then(res => {
        var data = res.data.data;
        var items = [
            { src: cover, thumb: cover, title: "Cover" }
        ];
        for (i = 0; i < data.length; i++) {
            items.push({
                src: url + data[i].isi,
                thumb: url + data[i].isi,
                title: data[i].no
            });
        }
        $('#app').flipBook({
            pages: items
        });
    }).catch(err => {
        console.log(err)
    })

    $('#app').flipBook({
        pages: [
            { src: "book/page1.jpg", thumb: "book/thumb1.jpg", title: "Cover" },
            { src: "book/page2.jpg", thumb: "book/thumb2.jpg", title: "Page two" },
            { src: "book/page3.jpg", thumb: "book/thumb3.jpg", title: "Page three" },
            { src: "book/page4.jpg", thumb: "book/thumb4.jpg", title: "" },
            { src: "book/page5.jpg", thumb: "book/thumb5.jpg", title: "Page five" },
            { src: "book/page6.jpg", thumb: "book/thumb6.jpg", title: "Page six" },
            { src: "book/page7.jpg", thumb: "book/thumb7.jpg", title: "Page seven" },
            { src: "book/page8.jpg", thumb: "book/thumb8.jpg", title: "Last" }
        ]
    })
}